# Implementation of classic arcade game Pong

import SimpleGUICS2Pygame.simpleguics2pygame as simplegui
import random

# initialize globals - pos and vel encode vertical info for paddles
WIDTH = 600
HEIGHT = 400       
BALL_RADIUS = 20
PAD_WIDTH = 8
PAD_HEIGHT = 80
HALF_PAD_WIDTH = PAD_WIDTH / 2
HALF_PAD_HEIGHT = PAD_HEIGHT / 2
LEFT = False
RIGHT = True
LEFT_KEY = simplegui.KEY_MAP["left"]
RIGHT_KEY = simplegui.KEY_MAP["right"]
paddle1_pos = [[0, HEIGHT/2 - HALF_PAD_HEIGHT], [0, HEIGHT/2 + HALF_PAD_HEIGHT], [PAD_WIDTH, HEIGHT/2 + HALF_PAD_HEIGHT],
                         [PAD_WIDTH, HEIGHT/2 - HALF_PAD_HEIGHT] ]

paddle2_pos = [[WIDTH, HEIGHT/2 - HALF_PAD_HEIGHT], [WIDTH, HEIGHT/2 + HALF_PAD_HEIGHT], [WIDTH - PAD_WIDTH, HEIGHT/2 + HALF_PAD_HEIGHT],
                         [WIDTH - PAD_WIDTH, HEIGHT/2 - HALF_PAD_HEIGHT]]

paddle1_vel = 0
paddle2_vel = 0

score1 = 0
score2 = 0

# ball_pos = [WIDTH / 2, HEIGHT / 2]
# ball_vel = [0, 0]

# initialize ball_pos and ball_vel for new bal in middle of table
# if direction is RIGHT, the ball's velocity is upper right, else upper left
def spawn_ball(direction):
    global ball_pos, ball_vel # these are vectors stored as lists
    ball_pos = [WIDTH/2, HEIGHT/2]
    ball_vel = [0, 0]
    ball_vel[0] = random.randrange(120, 140)
    ball_vel[1] = random.randrange(60, 180)
    # print "Random numbers", ball_vel    

    if direction == LEFT_KEY:
        ball_vel[0] = -ball_vel[0] / 60.0 # pixel per second
        ball_vel[1] = -ball_vel[1] / 60.0
        # print "Velocity dopo", ball_vel
    elif direction == RIGHT_KEY:
        # print "Velocity prima", ball_vel
        ball_vel[0] = ball_vel[0] / 60.0
        ball_vel[1] = -ball_vel[1] / 60.0
        # print "Velocity dopo", ball_vel
  

    ball_pos[0] += ball_vel[0]
    ball_pos[1] += ball_vel[1]
    # canvas.draw_circle(ball_pos, BALL_RADIUS, 2, "Yellow", "Orange")
    

# define event handlers
def new_game():
    global paddle1_pos, paddle2_pos, paddle1_vel, paddle2_vel  # these are numbers
    global score1, score2  # these are ints
    score1 = 0
    score2 = 0
    spawn_ball(simplegui.KEY_MAP["left"])

def draw(canvas):
    global score1, score2, paddle1_pos, paddle2_pos, ball_pos, ball_vel
    global paddle1_vel, paddle2_vel
 
        
    # draw mid line and gutters
    canvas.draw_line([WIDTH / 2, 0],[WIDTH / 2, HEIGHT], 1, "White")
    canvas.draw_line([PAD_WIDTH, 0],[PAD_WIDTH, HEIGHT], 1, "White")
    canvas.draw_line([WIDTH - PAD_WIDTH, 0],[WIDTH - PAD_WIDTH, HEIGHT], 1, "White")
        
    # update ball
    ball_pos[0] += ball_vel[0]
    ball_pos[1] += ball_vel[1]
    
    # collide and reflect 
    if ball_pos[0] <= BALL_RADIUS + PAD_WIDTH and ball_pos[1] >= paddle1_pos[0][1] - BALL_RADIUS and ball_pos[1] < paddle1_pos[1][1] + BALL_RADIUS:
        ball_vel[0] = - ball_vel[0] * 1.10
        ball_vel[1] = - ball_vel[1] * 1.10
        
    elif ball_pos[0] >= WIDTH - 1 - BALL_RADIUS - PAD_WIDTH and ball_pos[1] >= paddle2_pos[0][1] - BALL_RADIUS and ball_pos[1] < paddle2_pos[1][1] + BALL_RADIUS:
        ball_vel[0] = - ball_vel[0] * 1.10
        ball_vel[1] = - ball_vel[1] * 1.10
    
    elif ball_pos[0] <= BALL_RADIUS + PAD_WIDTH:
        score2 += 1
        spawn_ball(RIGHT_KEY)        
        # ball_vel[0] = - ball_vel[0] 
    elif ball_pos[0] >= WIDTH - 1 - BALL_RADIUS - PAD_WIDTH:
        score1 += 1
        spawn_ball(LEFT_KEY)
        # ball_vel[0] = - ball_vel[0]
    elif ball_pos[1] <= BALL_RADIUS:
        ball_vel[1] = - ball_vel[1]
    elif ball_pos[1] >= HEIGHT - 1 - BALL_RADIUS:
        ball_vel[1] = - ball_vel[1]

    
    # draw ball
    canvas.draw_circle(ball_pos, BALL_RADIUS, 2, "Yellow", "white")
    
    # update paddle's vertical position, keep paddle on the screen
    
    if paddle1_pos [0][1] <= 0: 
        paddle1_pos[0][1] += abs(paddle1_vel)
        paddle1_pos[1][1] += abs(paddle1_vel)
        paddle1_pos[2][1] += abs(paddle1_vel)
        paddle1_pos[3][1] += abs(paddle1_vel)
        
    elif paddle1_pos[0][1] >= HEIGHT - 1 - PAD_HEIGHT:
        paddle1_pos[0][1] -= abs(paddle1_vel)
        paddle1_pos[1][1] -= abs(paddle1_vel)
        paddle1_pos[2][1] -= abs(paddle1_vel)
        paddle1_pos[3][1] -= abs(paddle1_vel)
     
    elif paddle1_pos [0][1] > 0 and paddle1_pos[0][1] < HEIGHT - 1 - PAD_HEIGHT:
        paddle1_pos[0][1] += paddle1_vel
        paddle1_pos[1][1] += paddle1_vel
        paddle1_pos[2][1] += paddle1_vel
        paddle1_pos[3][1] += paddle1_vel

    
    if paddle2_pos [0][1] <= 0:
        paddle2_pos[0][1] += abs(paddle2_vel)
        paddle2_pos[1][1] += abs(paddle2_vel)
        paddle2_pos[2][1] += abs(paddle2_vel)
        paddle2_pos[3][1] += abs(paddle2_vel)
        
    elif paddle2_pos[0][1] >= HEIGHT - 1 - PAD_HEIGHT:
        paddle2_pos[0][1] -= abs(paddle2_vel)
        paddle2_pos[1][1] -= abs(paddle2_vel)
        paddle2_pos[2][1] -= abs(paddle2_vel)
        paddle2_pos[3][1] -= abs(paddle2_vel)
        
    elif paddle2_pos [0][1] > 0 and paddle2_pos[0][1] < HEIGHT - 1 - PAD_HEIGHT:
        paddle2_pos[0][1] += paddle2_vel
        paddle2_pos[1][1] += paddle2_vel
        paddle2_pos[2][1] += paddle2_vel
        paddle2_pos[3][1] += paddle2_vel


    # draw paddles
    canvas.draw_polygon(paddle1_pos, 1, 'Yellow', 'Yellow')  
    canvas.draw_polygon(paddle2_pos, 1, 'Red', 'Red')
    # draw scores
    canvas.draw_text(str(score1), (WIDTH / 2 - 30, 25), 30, 'Yellow')
    # draw scores
    canvas.draw_text(str(score2), (WIDTH / 2 + 15, 25), 30, 'Red')
    
def keydown(key):
    global paddle1_vel, paddle2_vel
    if key == simplegui.KEY_MAP["left"] or key == simplegui.KEY_MAP["right"]:
        spawn_ball(key)
    if key == simplegui.KEY_MAP["w"]:
        paddle1_vel = -120/60
    elif key == simplegui.KEY_MAP["s"]:
        paddle1_vel = 120/60
    if key == simplegui.KEY_MAP["up"]:
        paddle2_vel = -120/60
    elif key == simplegui.KEY_MAP["down"]:
        paddle2_vel = 120/60
                                  
   
def keyup(key):
    global paddle1_vel, paddle2_vel
    if key == simplegui.KEY_MAP["w"]:
        paddle1_vel = 0
    elif key == simplegui.KEY_MAP["s"]:
        paddle1_vel = 0
    if key == simplegui.KEY_MAP["up"]:
        paddle2_vel = 0
    elif key == simplegui.KEY_MAP["down"]:
        paddle2_vel = 0    


# create frame
frame = simplegui.create_frame("Pong", WIDTH, HEIGHT)
frame.set_draw_handler(draw)
frame.set_keydown_handler(keydown)
frame.set_keyup_handler(keyup)
frame.add_button("New game", new_game)


# start frame
new_game()
frame.start()


