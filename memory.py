# implementation of card game - Memory

# import simplegui
import SimpleGUICS2Pygame.simpleguics2pygame as simplegui
import random

state = 0
exposed_cards = [] # indexes of exposed cards
color = ['Red'] * 16
numbers = range(8) * 2
clicked_card = None
turns = 0


# helper function to initialize globals
def new_game():
    global state, numbers, turns, clicked_card, exposed_cards
    state = 0
    turns = 0
    clicked_card = None
    exposed_cards = []
    for i in range(16):
        color[i] = 'Red'
    label.set_text("Turns = " + str(turns))
    random.shuffle(numbers)
    # print "Numbers are:", numbers

     
# define event handlers
def mouseclick(pos):
    # add game state logic here
    global state, exposed_cards, clicked_card, color, turns
    clicked_card = pos[0] / 50
    
    
    if state == 0:
        if clicked_card not in exposed_cards:
            color[clicked_card] = 'black'
            exposed_cards.append(clicked_card)
            turns += 1
            # print "State:", state, "Exposed:", exposed_cards
            
            state = 1
        
    elif state == 1:
        if clicked_card not in exposed_cards:
            color[clicked_card] = 'black'
            exposed_cards.append(clicked_card)          
            # print "State:", state, "Exposed:", exposed_cards
            turns += 1
            state = 2
        
    else:
        if clicked_card not in exposed_cards:
           
            if numbers[exposed_cards[-1]] != numbers[exposed_cards[-2]]:
                color[exposed_cards[-2]] = 'Red'
                color[exposed_cards[-1]] = 'Red'
                exposed_cards.pop(-2)
                exposed_cards.pop(-1)
            # print "State:", state, "Exposed:", exposed_cards
            color[clicked_card] = 'black'
            exposed_cards.append(clicked_card)
            turns += 1
            state = 1
    label.set_text("Turns = " + str(turns))
    
                        
# cards are logically 50x100 pixels in size    
def draw(canvas):
    for i in range(16):
        canvas.draw_polygon([[50 * i, 0], 
                             [50 * (i + 1), 0], 
                             [50 * (i + 1), 100], 
                             [50 * i, 100]], 1, 'Yellow', color[i])
        
    for i in exposed_cards:               
        canvas.draw_text(str(numbers[i]), (20 + 50 * i, 60), 30, 'Yellow')


# create frame and add a button and labels
frame = simplegui.create_frame("Memory", 800, 100)
frame.add_button("Reset", new_game)
label = frame.add_label("Turns = 0")


# register event handlers
frame.set_mouseclick_handler(mouseclick)
frame.set_draw_handler(draw)

# get things rolling
new_game()
frame.start()

