"""
Cookie Clicker Simulator
"""

# import simpleplot
import math

# import user34_6oIxq9AgFE_0 as testsuite

# Used to increase the timeout, if necessary
# import codeskulptor
# codeskulptor.set_timeout(20)

import poc_clicker_provided as provided

# Constants
SIM_TIME = 10000000000.0

class ClickerState:
    """
    Simple class to keep track of the game state.
    """
    
    def __init__(self):
        self._total_cookies = 0.0
        self._current_cookies = 0.0
        self._current_time = 0.0
        self._cps = 1.0
        self._history = [(0.0, None, 0.0, 0.0)]
        
    def __str__(self):
        """
        Return human readable state
        """
        current_state = "\nTotal cookies = " + str(self._total_cookies) + "\nCurrent cookies = " + str(self._current_cookies) + "\nCurrent time = " + str(self._current_time) + "\nCPS = " + str(self._cps)
        
        return current_state
        
    def get_cookies(self):
        """
        Return current number of cookies 
        (not total number of cookies)
        
        Should return a float
        """
        return self._current_cookies
    
    def get_cps(self):
        """
        Get current CPS

        Should return a float
        """
        return self._cps
    
    def get_time(self):
        """
        Get current time

        Should return a float
        """
        return self._current_time
    
    def get_history(self):
        """
        Return history list

        History list should be a list of tuples of the form:
        (time, item, cost of item, total cookies)

        For example: (0.0, None, 0.0, 0.0)
        """
        return self._history

    def time_until(self, cookies):
        """
        Return time until you have the given number of cookies
        (could be 0 if you already have enough cookies)

        Should return a float with no fractional part
        """
        if self._current_cookies >= cookies:
            return 0.0
    
        else:
            return math.ceil( ( cookies - self._current_cookies) / self._cps )
        
    def wait(self, time):
        """
        Wait for given amount of time and update state

        Should do nothing if time <= 0
        """
        new_cookies = time * self._cps
        
        if time <= 0:
            return
        else:
            self._current_time += time
            self._current_cookies += new_cookies
            self._total_cookies += new_cookies
    
    def buy_item(self, item_name, cost, additional_cps):
        """
        Buy an item and update state

        Should do nothing if you cannot afford the item
        """
        if self._current_cookies < cost:
            return
        else:
            self._current_cookies -= cost
            self._cps += additional_cps
            self._history.append((self._current_time, item_name, cost, self._total_cookies))
   
    
def simulate_clicker(build_info, duration, strategy):
    """
    Function to run a Cookie Clicker game for the given
    duration with the given strategy.  Returns a ClickerState
    object corresponding to game.
    """
    build_info_clone = build_info.clone()
    clicker_state = ClickerState()     
    
    elapsed_time = 0.0
    
    while elapsed_time <= duration:
        if clicker_state.get_time() > duration:
            break
        
        time_left = duration - elapsed_time 
        item_to_purchase = strategy(clicker_state.get_cookies(), clicker_state.get_cps(), time_left, build_info_clone)
        
        if item_to_purchase == None:
            clicker_state.wait(time_left)
            break
        
        item_cost = build_info_clone.get_cost(item_to_purchase)
        item_cps = build_info_clone.get_cps(item_to_purchase)
        
        time_until = clicker_state.time_until(item_cost)   
        
        if time_until > time_left:
            clicker_state.wait(time_left)
            break
        
        else:
            clicker_state.wait(time_until)
            elapsed_time += time_until
            clicker_state.buy_item(item_to_purchase, item_cost, item_cps)
            build_info_clone.update_item(item_to_purchase)
        

    # Replace with your code
    return clicker_state


def strategy_cursor(cookies, cps, time_left, build_info):
    """
    Always pick Cursor!

    Note that this simplistic strategy does not properly check whether
    it can actually buy a Cursor in the time left.  Your strategy
    functions must do this and return None rather than an item you
    can't buy in the time left.
    """
    return "Cursor"

def strategy_none(cookies, cps, time_left, build_info):
    """
    Always return None

    This is a pointless strategy that you can use to help debug
    your simulate_clicker function.
    """
    return None

def strategy_cheap(cookies, cps, time_left, build_info):
    """
    Always return the cheapest

    This returns the cheapest strategy
    """
    cheapest_item = build_info.build_items()[0]
    
    for item in build_info.build_items():
        if build_info.get_cost(item) < build_info.get_cost(cheapest_item):
            cheapest_item = item
        
    if build_info.get_cost(cheapest_item) > cookies + cps * time_left:
        cheapest_item = None        
    
    return cheapest_item

def strategy_expensive(cookies, cps, time_left, build_info):
    """
    Always return the most expensive strategy

    This returns the most expensive strategy
    """
    available_resources = cookies + cps * time_left    
    expensive_item = None    
    for item in build_info.build_items():
        current_cost = build_info.get_cost(item)
        if available_resources >= current_cost:
            if expensive_item == None: 
                expensive_item = item
            elif build_info.get_cost(expensive_item) < current_cost: 
                expensive_item = item
    
    return expensive_item
    

def strategy_best(cookies, cps, time_left, build_info):
    """
    Always returns the best strategy

    This returns the best strategy
    """
    available_resources = cookies + cps * time_left    
    best_item = None
    best_value = 0.0
    
    for item in build_info.build_items():
        
        current_cost = build_info.get_cost(item)
        current_cps = build_info.get_cps(item)
        current_value = current_cps/current_cost
        
        
        
        if available_resources >= current_cost:
            if best_item == None: 
                best_item = item
                best_value = current_value
            elif best_value < current_value: 
                best_item = item
    
    return best_item
        
def run_strategy(strategy_name, time, strategy):
    """
    Run a simulation with one strategy
    """
    state = simulate_clicker(provided.BuildInfo(), time, strategy)
    print strategy_name, ":", state

    # Plot total cookies over time

    # Uncomment out the lines below to see a plot of total cookies vs. time
    # Be sure to allow popups, if you do want to see it

    # history = state.get_history()
    # history = [(item[0], item[3]) for item in history]
    # simpleplot.plot_lines(strategy_name, 1000, 400, 'Time', 'Total Cookies', [history], True)

def run():
    """
    Run the simulator.
    """    
    run_strategy("Cursor", SIM_TIME, strategy_cursor)

    # Add calls to run_strategy to run additional strategies
    # run_strategy("Cheap", SIM_TIME, strategy_cheap)
    run_strategy("\nExpensive", SIM_TIME, strategy_expensive)
    run_strategy("\nBest", SIM_TIME, strategy_best)
    
run()

